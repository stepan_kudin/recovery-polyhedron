#ifndef CONVEX_HALL_H
#define CONVEX_HALL_H

#include <iostream>
#include <stack>
#include <vector>
#include <list>
#include <algorithm>
#include <math.h>

#include "../lib/geometry_lib/vector_2d.h"

const double COMPARE_EPS = 1e-10;
const double PI = 3.14159265358979323846;

struct extendend_point
{
  unsigned int index;
  double polar_angle;
};


bool compare_points(vector_2d point_1, vector_2d point_2);
bool compare_polar_angle(extendend_point point_1, extendend_point point_2);

class convex_hall
{
public:
  convex_hall(std::vector<vector_2d> *_points);
  ~convex_hall();
  std::vector<vector_2d> get_convex_hall();

private:
  std::stack<vector_2d> stack;
  std::vector<vector_2d> *points;
  std::list<vector_2d>  set;

  vector_2d next_to_top();
  double get_polar_angle(const vector_2d &center, const vector_2d &point);
  void polar_angle_sort();
  void print_convex_hall();

};

#endif // CONVEX_HALL_H
