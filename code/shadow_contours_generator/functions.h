#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <math.h>
#include <vector>
#include <random>
#include <time.h>

#include "../lib/geometry_lib/vector_2d.h"
#include "../lib/geometry_lib/vector_3d.h"
#include "const.h"

void construct_plane(double angle, double &a, double &b, double &c, double &d);
void construct_transition_matrix(double *matrix, double angle);
void construct_linear_system_matrix(double *matrix, double a, double b);
void construct_right_hand_member(vector_3d &point, double d, double *right_hand_member);
void movement_points(std::vector<vector_2d> &points);
void multiplication_matrix_by_vector(double *matrix, int size_matrix, double *vector, double *result);
std::vector<vector_2d> merge_near_points(std::vector<vector_2d> &points);
unsigned int get_next_index(unsigned int index, unsigned int size);
unsigned int get_prev_index(unsigned int index, unsigned int size);
void polygon_smoothing(std::vector<vector_2d> &polygon);

#endif // FUNCTIONS_H
