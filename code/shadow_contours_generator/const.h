#ifndef CONST_H
#define CONST_H

// Радиус окружности.
const double R = 10.0;

// Порог слияния точек.
const double NEAR_RANGE_LIMIT = 1e-3;

// Максимальное отклонение координаты.
const double MAX_EPS = 1e-3;

#endif // CONST_H
