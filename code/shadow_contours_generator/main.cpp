#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <vector>

#include "../lib/geometry_lib/vector_2d.h"
#include "../lib/geometry_lib/vector_3d.h"
#include "../lib/io/original_model_reader.h"
#include "../lib/linear_system_solver/reflections_method.h"

#include "const.h"
#include "functions.h"
#include "convex_hall.h"

void multiplication_matrix_by_vector(double *matrix, int size_matrix, double *vector, double *result);
std::vector<vector_2d> merge_near_points(std::vector<vector_2d> &points);

// Параметров запуска 2 штуки, оба обязательны.
// Первый - путь до файла с моделью.
// Второй - количество контуров.
// Контуры генерируются с одинаковым шагом по полярному углу.
int main(int argc, char *argv[])
{
  if(argc != 3)
    {
      printf("Usage: %s <model name> <count of shadow contours>\n", argv[0]);
      return -1;
    }

  char *filename = argv[1];
  int count_shadow = atoi(argv[2]);

  if(count_shadow < 1)
    {
      printf("Count of shadow contours is positive number!\n");
      return -2;
    }

  std::vector<vector_3d> model_vertexes;
  if(original_model_vertexes_reader(filename, model_vertexes))
    {
      printf("Couldn't open file %s\n", filename);
      return -3;
    }

  printf("Work is begun...\n");

  double step_angle = 2 * PI / count_shadow;
  double current_angle = 0.0;
  int index;
  unsigned int current_point_index;
  // Коэффициенты уравнения плоскости.
  double a, b, c, d;
  // Матрица перехода.
  double *transition_matrix = new double[9];
  // Обратная матрица перехода.
  double *inverse_transition_matrix = new double[9];
  // Массив с проекциями точек.
  std::vector<vector_2d> projection_point(model_vertexes.size());
  // Матрица для линейной системы.
  double *matrix = new double[9];
  // Для правой части.
  double *right_hand_member = new double[3];
  // Вектор решения.
  double *result = new double[3];
  // Вектор с новыми координатами.
  double *new_coord = new double[3];
  // Выпуклая оболочка.
  convex_hall *polygon;

  FILE *shadows_contours = fopen(strcat(filename, "_contours.dat"), "w");

  fprintf(shadows_contours, "%d\n", count_shadow);

  for(index = 0; index < count_shadow; ++index)
    {
      // Построим плоскость, на которую будем проецировать.
      construct_plane(current_angle, a, b, c, d);

      // Построим матрицу перехода в систему координат, связанную с полученной плоскостью.
      // В этой системе координат у точек на этой плоскости z' = 0.
      // Отбросив Oz' получим локальную систему координат на плоскости, на которую проецируем.
      construct_transition_matrix(&transition_matrix[0], current_angle);

      // Посчитаем обратную матрицу
      if(reflections_method_inverse_matrix(transition_matrix, 3, inverse_transition_matrix))
        printf("Bad inverse matrix in %d step!\n", index);

      // В результате вычисления матрица перехода испортилась, зададим её занового.
      construct_transition_matrix(&transition_matrix[0], current_angle);

      // Проецируем точки.
      for(current_point_index = 0; current_point_index < model_vertexes.size(); ++current_point_index)
        {
          // Заполним матрицу и правую часть.
          construct_linear_system_matrix(&matrix[0], a, b);
          construct_right_hand_member(model_vertexes[current_point_index], d, &right_hand_member[0]);

          reflections_method_solver(matrix, 3, right_hand_member, result);

          // Новые координаты
          right_hand_member[0] = result[0] - R * cos(current_angle);
          right_hand_member[1] = result[1] - R * sin(current_angle);
          right_hand_member[2] = model_vertexes[current_point_index].z;
          multiplication_matrix_by_vector(inverse_transition_matrix, 3, right_hand_member, new_coord);

          // Сохраним точку в массив.
          projection_point[current_point_index].x = new_coord[0];
          projection_point[current_point_index].y = new_coord[1];
        }

      // Сольём ближайшие точки.
      std::vector<vector_2d> cleaned_set_points = merge_near_points(projection_point);

      // Пошевелим точки.
      movement_points(cleaned_set_points);

      // Построим выпуклую оболочку тени.
      polygon = new convex_hall(&cleaned_set_points);

      // Запишем результат.
      fprintf(shadows_contours, "%d %.15lf %.15lf %.15lf\n", index, R * cos(current_angle), R * sin(current_angle), 0.0);

      // Запишем тень.
      std::vector<vector_2d> current_shadow = polygon->get_convex_hall();
      polygon_smoothing(current_shadow);

      delete polygon;

      fprintf(shadows_contours, "%d\n", current_shadow.size());

      new_coord[0] = current_shadow[0].x;
      new_coord[1] = current_shadow[0].y;
      new_coord[2] = 0.0;

      multiplication_matrix_by_vector(transition_matrix, 3, new_coord, result);

      vector_3d prev_point, current_point;
      prev_point.x = result[0] + R * cos(current_angle);
      prev_point.y = result[1] + R * sin(current_angle);
      prev_point.z = result[2];

      if(fabs(a * prev_point.x + b * prev_point.y + d) > 1e-8)
        printf("Wrong point!\n");

      for(unsigned int shadow_point_index = 1; shadow_point_index < current_shadow.size(); ++shadow_point_index)
        {
          new_coord[0] = current_shadow[shadow_point_index].x;
          new_coord[1] = current_shadow[shadow_point_index].y;
          new_coord[2] = 0.0;

          multiplication_matrix_by_vector(transition_matrix, 3, new_coord, result);

          current_point.x = result[0] + R * cos(current_angle);
          current_point.y = result[1] + R * sin(current_angle);
          current_point.z = result[2];

          if(fabs(a * current_point.x + b * current_point.y + d) > 1e-8)
            printf("Wrong point!\n");

          fprintf(shadows_contours, "%d %lf ", 0, 1.0);
          fprintf(shadows_contours, "%.15lf %.15lf %.15lf ", prev_point.x, prev_point.y, prev_point.z);
          fprintf(shadows_contours, "%.15lf %.15lf %.15lf\n", current_point.x, current_point.y, current_point.z);

          prev_point = current_point;
        }

      new_coord[0] = current_shadow[0].x;
      new_coord[1] = current_shadow[0].y;
      new_coord[2] = 0.0;

      multiplication_matrix_by_vector(transition_matrix, 3, new_coord, result);

      current_point.x = result[0] + R * cos(current_angle);
      current_point.y = result[1] + R * sin(current_angle);
      current_point.z = result[2];

      fprintf(shadows_contours, "%d %lf ", 0, 1.0);
      fprintf(shadows_contours, "%.15lf %.15lf %.15lf ", prev_point.x, prev_point.y, prev_point.z);
      fprintf(shadows_contours, "%.15lf %.15lf %.15lf\n", current_point.x, current_point.y, current_point.z);

      // Угол для следующего шага.
      current_angle += step_angle;

      current_shadow.clear();
      cleaned_set_points.clear();
    }

  fclose(shadows_contours);

  printf("Work is complete!\n");

  delete[] transition_matrix;
  delete[] inverse_transition_matrix;
  delete[] matrix;
  delete[] right_hand_member;
  delete[] result;
  delete[] new_coord;

  return 0;
}
