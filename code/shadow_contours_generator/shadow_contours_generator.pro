TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp \
    ../lib/io/original_model_reader.cpp \
    ../lib/geometry_lib/polygon_3d.cpp \
    ../lib/geometry_lib/polygon_2d.cpp \
    ../lib/geometry_lib/gl.cpp \
    ../lib/linear_system_solver/reflections_method.cpp \
    convex_hall.cpp \
    functions.cpp

HEADERS += \
    ../lib/io/original_model_reader.h \
    ../lib/geometry_lib/vector_3d.h \
    ../lib/geometry_lib/vector_2d.h \
    ../lib/geometry_lib/polygon_3d.h \
    ../lib/geometry_lib/polygon_2d.h \
    ../lib/geometry_lib/line_3d.h \
    ../lib/geometry_lib/gl.h \
    ../lib/linear_system_solver/reflections_method.h \
    convex_hall.h \
    functions.h \
    const.h

