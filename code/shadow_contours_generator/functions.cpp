#include "functions.h"

void construct_plane(double angle, double &a, double &b, double &c, double &d)
{
  a = R * cos(angle);
  b = R * sin(angle);
  c = 0.0;
  d = (-1) * R * R;
}

void construct_transition_matrix(double *matrix, double angle)
{
  matrix[0] = (-1) * sin(angle);
  matrix[1] = 0.0;
  matrix[2] = cos(angle);
  matrix[3] = cos(angle);
  matrix[4] = 0.0;
  matrix[5] = sin(angle);
  matrix[6] = 0.0;
  matrix[7] = 1.0;
  matrix[8] = 0.0;
}

void construct_linear_system_matrix(double *matrix, double a, double b)
{
  matrix[0] = a;
  matrix[1] = b;
  matrix[2] = 0.0;
  matrix[3] = 1.0;
  matrix[4] = 0.0;
  matrix[5] = (-1) * a;
  matrix[6] = 0.0;
  matrix[7] = 1.0;
  matrix[8] = (-1.0) * b;
}

void construct_right_hand_member(vector_3d &point, double d, double *right_hand_member)
{
  right_hand_member[0] = (-1) * d;
  right_hand_member[1] = point.x;
  right_hand_member[2] = point.y;
}

void movement_points(std::vector<vector_2d> &points)
{
  double buffer;
  int sign;
  std::default_random_engine engine(time(0));
  std::uniform_real_distribution<> deflection(0, MAX_EPS);
  for(unsigned int current_point_index = 0; current_point_index < points.size(); ++current_point_index)
    {
      vector_2d new_point;

      double new_r = deflection(engine);
      std::uniform_real_distribution<> deflection_2(0, new_r);

      buffer = deflection(engine);
      if (buffer >= MAX_EPS / 2.0)
        sign = 1;
      else
        sign = -1;

      buffer = deflection_2(engine);
      new_point.x = points[current_point_index].x + sign * buffer;

      buffer = deflection(engine);
      if (buffer >= MAX_EPS / 2.0)
        sign = 1;
      else
        sign = -1;

      new_point.y = sign * sqrt(new_r * new_r - (new_point.x - points[current_point_index].x) * (new_point.x - points[current_point_index].x)) +
              points[current_point_index].y;

      points[current_point_index] = new_point;
    }
}


void multiplication_matrix_by_vector(double *matrix, int size_matrix, double *vector, double *result)
{
  int index_1, index_2;
  double buffer;

  for(index_1 = 0; index_1 < size_matrix; ++index_1)
    {
      buffer = 0.0;
      for(index_2 = 0; index_2 < size_matrix; ++index_2)
        buffer += matrix[index_1 * size_matrix + index_2] * vector[index_2];
      result[index_1] = buffer;
    }
}


std::vector<vector_2d> merge_near_points(std::vector<vector_2d> &points)
{
  std::vector<vector_2d> result;
  char *mask = new char[points.size()];
  unsigned int index_1, index_2;

  for(index_1 = 0; index_1 < points.size(); ++index_1)
    mask[index_1] = 1;

  for(index_1 = 0; index_1 < points.size(); ++index_1)
    {
      if(mask[index_1])
        {
          result.push_back(points[index_1]);

          for(index_2 = index_1 + 1; index_2 < points.size(); ++index_2)
            {
              if((points[index_2] - points[index_1]).length() < NEAR_RANGE_LIMIT)
                mask[index_2] = 0;
            }
        }
    }

  delete[] mask;

  return result;
}

unsigned int get_next_index(unsigned int index, unsigned int size)
{
  unsigned int buffer = index + 1;

  if(buffer < size)
    return buffer;

  return 0;
}

unsigned int get_prev_index(unsigned int index, unsigned int size)
{
  if(index == 0)
    return size - 1;

  return index - 1;
}

void polygon_smoothing(std::vector<vector_2d> &polygon)
{
  double *angles = new double[polygon.size()];
  vector_2d a, b;

  unsigned int prev, current, next;
  auto it = polygon.begin();
  unsigned int deleted = 0;

  for(current = 0; current < polygon.size(); ++current)
    {
      prev = get_prev_index(current, polygon.size());
      next = get_next_index(current, polygon.size());

      a = polygon[next] - polygon[current];
      b = polygon[current] - polygon[prev];

      angles[current] = acos((a * b) / (a.length() * b.length()));
    }

  for(current = 0; current < polygon.size(); ++current)
    {
      if(angles[current] < (3.14159265358979323846 / 180))
        {
          it = polygon.begin() + current - deleted;
          polygon.erase(it);
          ++deleted;
        }
    }

if(deleted)
  polygon_smoothing(polygon);
}
