#include "convex_hall.h"

convex_hall::convex_hall(std::vector<vector_2d> *_points)
{
  points = _points;

  // Отсортируем точки так, чтобы на первой позиции стояла точка с наименьшей координатой y, и,
  // если таких точек несколько, наиболее левая.
  sort(points->begin(), points->end(), compare_points);

  polar_angle_sort();

  std::list<vector_2d>::iterator set_it = set.begin();

  stack.push((*points)[0]);
  stack.push(*set_it);
  ++set_it;
  stack.push(*set_it);
  ++set_it;

  while(set_it != set.end())
    {
      while((*set_it - next_to_top()) % (stack.top() - next_to_top()) >= 0.0)
        stack.pop();

      stack.push(*set_it);
      ++set_it;
    }

//  print_convex_hall();
}

convex_hall::~convex_hall()
{
  set.clear();
}

std::vector<vector_2d> convex_hall::get_convex_hall()
{
  std::vector<vector_2d> result(stack.size());
  unsigned int index = stack.size() - 1;

  while(stack.size() > 0)
    {
      result[index] = stack.top();
      stack.pop();
      --index;
    }

  return result;
}

vector_2d convex_hall::next_to_top()
{
  vector_2d buffer;
  vector_2d result;

  buffer = stack.top();
  stack.pop();
  result = stack.top();
  stack.push(buffer);

  return result;
}

// Вычисление полярного угла относительно точки center.
double convex_hall::get_polar_angle(const vector_2d &center, const vector_2d &point)
{
  double x, y;

  x = point.x - center.x;
  y = point.y - center.y;

  if((x > COMPARE_EPS || x < -1.0 * COMPARE_EPS) && y > COMPARE_EPS)
    return atan2(y, x);
  if(fabs(x) > COMPARE_EPS && y < -1 * COMPARE_EPS)
    return atan2(y, x) + 2.0 * PI;
  if(fabs(x) < COMPARE_EPS && y > COMPARE_EPS)
    return PI / 2.0;
  if(fabs(x) < COMPARE_EPS && y < -1 * COMPARE_EPS)
    return 3.0 * PI / 2;
  if(fabs(x) < COMPARE_EPS && fabs(y) < COMPARE_EPS)
    return 0.0;
  if(fabs(y) < COMPARE_EPS && x > COMPARE_EPS)
    return 0.0;
  if(fabs(y) < COMPARE_EPS && x < -1.0 * COMPARE_EPS)
    return PI;

  return 0.0;
}

// Сортировка массива точек по полярному углу относительно центра.
void convex_hall::polar_angle_sort()
{
  unsigned int index;
  std::vector<extendend_point> raw_points(points->size() - 1);

  for(index = 1; index < points->size(); ++index)
    {
      raw_points[index - 1].index = index;
      raw_points[index - 1].polar_angle = get_polar_angle((*points)[0], (*points)[index]);
    }

  sort(raw_points.begin(), raw_points.end(), compare_polar_angle);

  double current_angle;
  double current_range, new_range;
  unsigned int current_index;
  unsigned int size = points->size() - 1;

  current_angle = raw_points[0].polar_angle;
  current_range = sqrt(((*points)[0].x - (*points)[raw_points[0].index].x) * ((*points)[0].x - (*points)[raw_points[0].index].x) +
      ((*points)[0].y - (*points)[raw_points[0].index].y) * ((*points)[0].y - (*points)[raw_points[0].index].y));
  current_index = 0;

  for(index = 1; index < size; ++index)
    {
      if(fabs(current_angle - raw_points[index].polar_angle) < COMPARE_EPS)
        {
          new_range = sqrt(((*points)[0].x - (*points)[raw_points[index].index].x) * ((*points)[0].x - (*points)[raw_points[index].index].x) +
              ((*points)[0].y - (*points)[raw_points[index].index].y) * ((*points)[0].y - (*points)[raw_points[index].index].y));
          if(new_range > current_range)
            {
              current_index = index;
              current_range = new_range;
            }
        }
      else
        {
          set.push_back((*points)[raw_points[current_index].index]);

          current_index = index;
          current_angle = raw_points[index].polar_angle;
          current_range = sqrt(((*points)[0].x - (*points)[raw_points[index].index].x) * ((*points)[0].x - (*points)[raw_points[index].index].x) +
              ((*points)[0].y - (*points)[raw_points[index].index].y) * ((*points)[0].y - (*points)[raw_points[index].index].y));
        }
    }
  set.push_back((*points)[raw_points[current_index].index]);
}

void convex_hall::print_convex_hall()
{
  while(stack.size() > 0)
    {
      vector_2d buffer = stack.top();
      std::cout << buffer.x << " " << buffer.y << std::endl;
      stack.pop();
    }
}

bool compare_points(vector_2d point_1, vector_2d point_2)
{
  if(point_1.y < point_2.y || (fabs(point_1.y - point_2.y) < COMPARE_EPS && point_1.x < point_2.x))
    return true;

  return false;
}

bool compare_polar_angle(extendend_point point_1, extendend_point point_2)
{
  return point_1.polar_angle < point_2.polar_angle;
}
