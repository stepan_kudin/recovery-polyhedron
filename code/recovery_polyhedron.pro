TEMPLATE = app
CONFIG += console
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    lib/geometry_lib/polygon_3d.cpp \
    lib/geometry_lib/polygon_2d.cpp \
    lib/geometry_lib/gl.cpp \
    lib/functions/lines_classifier.cpp \
    lib/linear_system_solver/reflections_method.cpp \
    lib/io/shadow_contours_reader.cpp \
    lib/io/model_polyhedron_reader.cpp \
    lib/checker/checker_vertices.cpp \
    lib/structures/shot_model.cpp \
    lib/io/original_model_reader.cpp \
    lib/tests/result_tests.cpp \
    lib/functions/shadow_contours_vertexes_diagram.cpp \
    lib/functions/diagram_clasterization.cpp \
    lib/functions/math_functions.cpp \
    lib/functions/finding_vertices_polyhedron.cpp \
    lib/structures/polyhedra_model.cpp

HEADERS += \
    lib/geometry_lib/vector_3d.h \
    lib/geometry_lib/vector_2d.h \
    lib/geometry_lib/const.h \
    lib/geometry_lib/line_3d.h \
    lib/geometry_lib/vector_3d.h \
    lib/geometry_lib/vector_2d.h \
    lib/geometry_lib/polygon_3d.h \
    lib/geometry_lib/polygon_2d.h \
    lib/geometry_lib/line_3d.h \
    lib/geometry_lib/gl.h \
    lib/const.h \
    lib/functions/lines_classifier.h \
    lib/linear_system_solver/reflections_method.h \
    lib/io/shadow_contours_reader.h \
    lib/io/model_polyhedron_reader.h \
    lib/checker/checker_vertices.h \
    lib/structures/shot_model.h \
    lib/io/original_model_reader.h \
    lib/tests/result_tests.h \
    lib/functions/shadow_contours_vertexes_diagram.h \
    lib/structures/diagram_point.h \
    lib/functions/diagram_clasterization.h \
    lib/structures/frame_point.h \
    lib/functions/math_functions.h \
    lib/functions/finding_vertices_polyhedron.h \
    lib/structures/vertex_cluster.h \
    lib/structures/point_for_forel.h \
    lib/structures/polyhedra_model.h

