#ifndef CHECKER_VERTICES_H
#define CHECKER_VERTICES_H

#include <math.h>

#include <list>

#include "lib/geometry_lib/vector_3d.h"

#include "lib/structures/vertex_cluster.h"
#include "lib/structures/polyhedra_model.h"

int checker_vertices(vector_3d *vertices, unsigned int size_vertices, vector_3d *example_vertices,
                     unsigned int size_example_vertices, int *result, double &max_dist, double &min_distanse);

int check_vertices(std::list<vertex_cluster> &vertices, polyhedra_model *model,
                   double &global_min_dist, double &global_max_dist);

#endif // CHECKER_VERTICES_H
