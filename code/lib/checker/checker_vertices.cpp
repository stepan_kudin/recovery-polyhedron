#include "checker_vertices.h"

int checker_vertices(vector_3d *vertices, unsigned int size_vertices, vector_3d *example_vertices,
                     unsigned int size_example_vertices, int *result, double &max_dist, double &min_distanse)
{
  double dist, num, absolute_max_dist;
  unsigned int index_1, index_2;

  for(index_1 = 0; index_1 < size_vertices; ++index_1)
    {
      num = -1;

      for(index_2 = 0; index_2 < size_example_vertices; ++index_2)
        {
          dist = sqrt((vertices[index_1].x - example_vertices[index_2].x) * (vertices[index_1].x - example_vertices[index_2].x) +
                      (vertices[index_1].y - example_vertices[index_2].y) * (vertices[index_1].y - example_vertices[index_2].y) +
                      (vertices[index_1].z - example_vertices[index_2].z) * (vertices[index_1].z - example_vertices[index_2].z));

          if(dist < max_dist)
            {
              max_dist = dist;
              num = index_2;
            }
        }
      result[index_1] = num;
    }

  absolute_max_dist = 0.0;
  double absolute_min_dist = 1e10;
  for(index_1 = 0; index_1 < size_vertices; ++index_1)
    {
      dist = sqrt((vertices[index_1].x - example_vertices[result[index_1]].x) * (vertices[index_1].x - example_vertices[result[index_1]].x) +
                  (vertices[index_1].y - example_vertices[result[index_1]].y) * (vertices[index_1].y - example_vertices[result[index_1]].y) +
                  (vertices[index_1].z - example_vertices[result[index_1]].z) * (vertices[index_1].z - example_vertices[result[index_1]].z));

      if(dist > absolute_max_dist)
        absolute_max_dist = dist;
      if(dist < absolute_min_dist)
        absolute_min_dist = dist;
    }

  max_dist = absolute_max_dist;
  min_distanse = absolute_min_dist;

  return 0;
}

int check_vertices(std::list<vertex_cluster> &vertices, polyhedra_model *model,
                   double &global_min_dist, double &global_max_dist)
{
  std::vector<bool> not_processed(model->vertices.size(), true);
  unsigned int index;
  double min_dist, current_dist;
  unsigned int current_near_point;
  bool iteration_flag;

  global_min_dist = 1e6;
  global_max_dist = 0.0;

  for(auto it : vertices)
    {
      min_dist = 1e6;
      iteration_flag = false;

      for(index = 0; index < model->vertices.size(); ++index)
        {
          if(not_processed[index])
            {
              current_dist = (it.center - model->vertices[index]).length();
              iteration_flag = true;

              if(current_dist < min_dist)
                {
                  min_dist = current_dist;
                  current_near_point = index;
                }
            }
        }

      if(iteration_flag)
        {
          not_processed[current_near_point] = false;

          if(global_min_dist > min_dist)
            global_min_dist = min_dist;

          if(global_max_dist < min_dist)
            global_max_dist = min_dist;
        }
    }

  return 0;
}
