#ifndef DIAGRAM_POINT_H
#define DIAGRAM_POINT_H

#include "lib/geometry_lib/vector_2d.h"

class diagram_point
{
public:
  diagram_point(): point(0,0), frame_index(0), vertex_index(0) {}
  diagram_point(vector_2d _point, unsigned int _frame_index, unsigned int _vertex_index):
    point(_point.x, point.y), frame_index(_frame_index), vertex_index(_vertex_index)
  {}

  vector_2d point;
  unsigned int frame_index;
  unsigned int vertex_index;
};

#endif // DIAGRAM_POINT_H
