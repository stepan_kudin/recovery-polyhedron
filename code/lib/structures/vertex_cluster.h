#ifndef VERTEX_CLUSTER_H
#define VERTEX_CLUSTER_H

#include <vector>

#include "lib/geometry_lib/vector_3d.h"

#include "lib/structures/point_for_forel.h"

struct vertex_cluster
{
  vector_3d center;

  std::vector<point_for_forel> points;
};

#endif // VERTEX_CLUSTER_H
