#include "shot_model.h"

shot_model::shot_model()
{
}

shot_model::~shot_model()
{
  vertexes.clear();
  edges.clear();
}

int shot_model::get_index_point(vector_3d point)
{
  unsigned int index;
  vector_3d buffer;

  for(index = 0; index < vertexes.size(); ++index)
    {
      buffer = vertexes[index] - point;

      if(buffer.length() < SENSITIVITY_DUPLICATES)
        return index;
    }

  return -1;
}
