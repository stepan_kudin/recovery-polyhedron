#ifndef POLYHEDRA_MODEL_H
#define POLYHEDRA_MODEL_H

#include <stdio.h>

#include <vector>

#include "lib/geometry_lib/vector_3d.h"

class polyhedra_model
{
public:
  polyhedra_model();
  ~polyhedra_model();

  int read_model(const char *filename);

  std::vector<vector_3d> vertices;
  std::vector<std::pair<unsigned int, unsigned int>> edges;
};

#endif // POLYHEDRA_MODEL_H
