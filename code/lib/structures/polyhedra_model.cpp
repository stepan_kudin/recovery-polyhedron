#include "polyhedra_model.h"

polyhedra_model::polyhedra_model()
{
}

polyhedra_model::~polyhedra_model()
{
  vertices.clear();
  edges.clear();
}

int polyhedra_model::read_model(const char *filename)
{
  FILE *model = fopen(filename, "r");

  if(model == NULL)
    {
      printf("Couldn't open file %s!\n", filename);
      return -1;
    }

  unsigned int count_vertices, count_facets;
  unsigned int index_1, index_2, current_point, prev_point, first_point;
  unsigned count_edges;
  unsigned int read_int_buffer;
  vector_3d read_vector_3d_buffer;
  double read_double_buffer;

  // Считаем количество вершин и граней.
  if(fscanf(model, "%u %u", &count_vertices, &count_facets) != 2)
    {
      printf("Read error!\n");
      return -2;
    }

  // Считаем вершины.
  vertices.reserve(count_vertices);
  for(index_1 = 0; index_1 < count_vertices; ++index_1)
    {
      if(fscanf(model, "%u %lf %lf %lf", &read_int_buffer, &read_vector_3d_buffer.x,
                &read_vector_3d_buffer.y, &read_vector_3d_buffer.z) != 4)
        {
          printf("Read error!\n");
          return -3;
        }

      vertices.push_back(read_vector_3d_buffer);
    }

  // Считаем рёбра.
  for(index_1 = 0; index_1 < count_facets; ++index_1)
    {
      if(fscanf(model, "%u %u %lf %lf %lf %lf", &read_int_buffer, &count_edges, &read_double_buffer,
                &read_double_buffer, &read_double_buffer, &read_double_buffer) != 6)
        {
          printf("Read error!\n");
          return -4;
        }

      if(fscanf(model, "%u", &prev_point) != 1)
        {
          printf("Read error!\n");
          return -5;
        }

      first_point = prev_point;

      for(index_2 = 1; index_2 < count_edges; ++index_2)
        {
          if(fscanf(model, "%u", &current_point) != 1)
            {
              printf("Read error!\n");
              return -6;
            }

          edges.emplace_back(prev_point, current_point);

          prev_point = current_point;
        }

      edges.emplace_back(current_point, first_point);
    }

  return 0;
}
