#ifndef POINT_FOR_FOREL_H
#define POINT_FOR_FOREL_H

#include "lib/geometry_lib/vector_3d.h"

#include "lib/structures/frame_point.h"

class point_for_forel
{
public:
  point_for_forel() : point(), parent_1(), parent_2() {}
  point_for_forel(vector_3d _point, frame_point _parent_1, frame_point _parent_2)
  {
    point = _point;
    parent_1 = _parent_1;
    parent_2 = _parent_2;
  }

  vector_3d point;
  frame_point parent_1;
  frame_point parent_2;
};

#endif // POINT_FOR_FOREL_H
