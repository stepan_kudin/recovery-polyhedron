#ifndef SHOT_MODEL_H
#define SHOT_MODEL_H

#include <math.h>

#include <vector>

#include "../geometry_lib/vector_3d.h"
#include "../const.h"

class shot_model
{
public:
  shot_model();
  ~shot_model();

  vector_3d projection_vector;
  std::vector<vector_3d> vertexes;
  std::vector< std::pair<int, int> > edges;

  // Точка в списке вершин? Если да, её номер
  int get_index_point(vector_3d point);
};

#endif // SHOT_MODEL_H
