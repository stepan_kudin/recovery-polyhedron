#ifndef FRAME_POINT_H
#define FRAME_POINT_H

class frame_point
{
public:
  frame_point(): frame_index(0), vertex_index(0) {}
  frame_point(unsigned int _frame_index, unsigned int _vertex_index):
    frame_index(_frame_index), vertex_index(_vertex_index)
  {}

  unsigned int frame_index;
  unsigned int vertex_index;
};

#endif // FRAME_POINT_H
