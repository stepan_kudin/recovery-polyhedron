#include "lines_classifier.h"

double distance_between_line_and_point(line_3d line, vector_3d point)
{
  line.directing_vector.norm();
  return sqrt ((point.x - line.point.x) * (point.x - line.point.x) * (1 - line.directing_vector.x * line.directing_vector.x) +
               (point.y - line.point.y) * (point.y - line.point.y) * (1 - line.directing_vector.y * line.directing_vector.y) +
               (point.z - line.point.z) * (point.z - line.point.z) * (1 - line.directing_vector.z * line.directing_vector.z) -
               2 * (point.x * point.y - point.x * line.point.y - point.y * line.point.x + line.point.x * line.point.y) *
               line.directing_vector.x * line.directing_vector.y -
               2 * (point.x * point.z - point.x * line.point.z - point.z * line.point.x + line.point.x * line.point.z) *
               line.directing_vector.x * line.directing_vector.z -
               2 * (point.y * point.z - point.y * line.point.z - point.z * line.point.y + line.point.y * line.point.z) *
               line.directing_vector.y * line.directing_vector.z);
}

int search_nearest_point_lines(line_3d *lines, int count, vector_3d &result)
{
  // Элементы матрицы
  double a_11 = 0.0, a_12 = 0.0, a_13 = 0.0,
         a_21 = 0.0, a_22 = 0.0, a_23 = 0.0,
         a_31 = 0.0, a_32 = 0.0, a_33 = 0.0;
  // Элементы правой части
  double b_1 = 0.0, b_2 = 0.0, b_3 = 0.0;
  double buffer;
  // Определители
  double delta;

  // Построим матрицу линейной системы и её правую часть
  for(int index = 0; index < count; ++index)
    {
      // Нормируем направляющий вектор прямой
      lines[index].directing_vector.norm();

      buffer = 1 - lines[index].directing_vector.x * lines[index].directing_vector.x;
      a_11  += buffer;
      b_1   += lines[index].point.x * buffer;

      buffer = lines[index].directing_vector.x * lines[index].directing_vector.y;
      a_12  -= buffer;
      b_1   -= lines[index].point.y * buffer;
      b_2   -= lines[index].point.x * buffer;

      buffer = lines[index].directing_vector.x * lines[index].directing_vector.z;
      a_13  -= buffer;
      b_1   -= lines[index].point.z * buffer;
      b_3   -= lines[index].point.x * buffer;

      buffer = 1 - lines[index].directing_vector.y * lines[index].directing_vector.y;
      a_22  += buffer;
      b_2   += lines[index].point.y * buffer;

      buffer = lines[index].directing_vector.y * lines[index].directing_vector.z;
      a_23  -= buffer;
      b_2   -= lines[index].point.z * buffer;
      b_3   -= lines[index].point.y * buffer;

      buffer = 1 - lines[index].directing_vector.z * lines[index].directing_vector.z;
      a_33  += buffer;
      b_3   += lines[index].point.z * buffer;
    }

  a_21 = a_12;
  a_31 = a_13;
  a_32 = a_23;

  delta = a_11 * a_22 * a_33 + a_21 * a_32 * a_13 + a_12 * a_23 * a_31 -
      a_13 * a_22 * a_31 - a_23 * a_32 * a_11 - a_12 * a_21 * a_33;

  if(fabs(delta) < EPS)
    return -1;

  // Перенесём всё в матрицу и правую часть
  double *matrix = new double[9];
  double *right_hand_member = new double[3];
  double *results = new double[3];

  matrix[0] = a_11;
  matrix[1] = a_12;
  matrix[2] = a_13;

  matrix[3] = a_21;
  matrix[4] = a_22;
  matrix[5] = a_23;

  matrix[6] = a_31;
  matrix[7] = a_32;
  matrix[8] = a_33;

  right_hand_member[0] = b_1;
  right_hand_member[1] = b_2;
  right_hand_member[2] = b_3;

  reflections_method_solver(matrix, 3, right_hand_member, results);

  result.x = results[0];
  result.y = results[1];
  result.z = results[2];

  delete[] matrix;
  delete[] right_hand_member;
  delete[] results;

  return 0;
}

std::vector< std::list<int> > lines_classifier(line_3d *lines, int lines_size)
{
  std::vector< std::list<int> > result;
  std::stack<int> first_stack, second_stack/*, current_stack*/;
  std::stack<int> *current_stack;
  std::list<int> lines_list;
  std::list<int>::iterator lines_list_iterator;
  line_3d *lines_buffer;
  vector_3d nearest_point;
  int index;
  int counter, top, current_stack_num, lines_buffer_size;
  double distance, max_distance;

  // Инициализируем первый стек
  for(index = 0; index < lines_size; ++index)
    first_stack.push(index);

  // Инициализация цикла
  counter = lines_size;
  current_stack = &first_stack;
  current_stack_num = 1;
  lines_buffer = new line_3d[lines_size];

  // Цикл по количеству необработанных элементов
  while(counter > 0)
    {
      std::cout << "Counter = " << counter << std::endl;
      // Перебор текущего стека
      while(!current_stack->empty())
        {
          // Получим элемент и удалим его из стека
          top = current_stack->top();
          current_stack->pop();
//          std::cout << "Current line = " << top << std::endl;

          // Обработаем точку
          if(!lines_list.empty())
            {
              // Здесь обрабатываем 2 случая, список не пуст

              // Заполним буффер с прямыми. Для последней прямой из буффера не известно, принадлежит ли она классу
              lines_buffer_size = lines_list.size();
              lines_list_iterator = lines_list.begin();
              for(index = 0; index < lines_buffer_size; ++index)
                {
                  lines_buffer[index] = lines[*lines_list_iterator];
                  ++lines_list_iterator;
                }
              lines_buffer[index] = lines[top];
              ++lines_buffer_size;

              // Найдём ближайшую точку
              if(search_nearest_point_lines(lines_buffer, lines_buffer_size, nearest_point) == -1)
                {
//                  std::cout << "Parallel!" << std::endl;
                  // Перекладываем во второй стек
                  if(current_stack_num == 1)
                    {
                      second_stack.push(top);
                    }
                  else
                    {
                      first_stack.push(top);
                    }
                  continue;
                }


              // Найдём максимальное расстояние
              max_distance = 0.0;
              for(index = 0; index < lines_buffer_size; ++index)
                {
                  distance = distance_between_line_and_point(lines_buffer[index], nearest_point);

                  if(distance > max_distance)
                    max_distance = distance;
                }

              if(max_distance < ASSUMPTIVE_ERROR)
                {
                  // Добавляем в список
                  lines_list.push_back(top);
                  --counter;
//                  std::cout << "Point is added!" << std::endl;
//                  std::cout << counter << std::endl;
                }
              else
                {
                  // Перекладываем во второй стек
                  if(current_stack_num == 1)
                    {
                      second_stack.push(top);
                    }
                  else
                    {
                      first_stack.push(top);
                    }

//                  std::cout << "Point is not added!" << std::endl;
                }
            }
          else
            {
              // Начинаем список, это первая прямая нового класса
              lines_list.push_back(top);
              --counter;
//              std::cout << "Point is added!" << std::endl;
//              std::cout << counter << std::endl;
            }
        }

      // Сохраняем класс
      result.push_back(lines_list);

      // Очищаем буферный список
      /*while(!lines_list.empty())
        lines_list.pop_front();*/
      lines_list.clear();

      if(!current_stack->empty())
        {
          std::cout << "Warning!" << std::endl;
          char bf;
          std::cin >> bf;
        }

      // Меняем текущий стек
      if(current_stack_num == 1)
        {
          current_stack = &second_stack;
          current_stack_num = 2;
        }
      else
        {
          current_stack = &first_stack;
          current_stack_num = 1;
        }
    }

  delete[] lines_buffer;

  return result;
}

unsigned int get_next_shot(std::vector<shot_model> &model_shots, unsigned int index)
{
  if(index + 1 < model_shots.size())
    return index + 1;
  else
    return 0;
}

unsigned int get_prev_shot(std::vector<shot_model> &model_shots, unsigned int index)
{
  int res = index - 1;
  if(res < 0)
    return model_shots.size() - 1;
  else
    return index - 1;
}

void converter_shots_to_projection_lines_arrays(std::vector<shot_model> &model_shots,
                                                std::vector< std::vector< std::pair <line_3d, bool> > > &projection_lines_arrays)
{
  std::vector<shot_model>::iterator shots_iterator;
  std::vector< std::pair<line_3d, bool> > projection_lines_array_buffer;
  std::pair<line_3d, bool> line_buffer;
  unsigned int index;

  line_buffer.second = false;

  for(shots_iterator = model_shots.begin(); shots_iterator != model_shots.end(); ++shots_iterator)
    {
      line_buffer.first.directing_vector = (*shots_iterator).projection_vector;

      for(index = 0; index < (*shots_iterator).vertexes.size(); ++index)
        {
          line_buffer.first.point = (*shots_iterator).vertexes[index];
          projection_lines_array_buffer.push_back(line_buffer);
        }

      projection_lines_arrays.push_back(projection_lines_array_buffer);
      projection_lines_array_buffer.clear();
    }
}

double distanse_between_lines_cluster_and_points(line_3d *lines_cluster, int size_lines_cluster, vector_3d point)
{
  int index;
  double result = -1.0;
  double buffer;

  for(index = 0; index < size_lines_cluster; ++index)
    {
      buffer = distance_between_line_and_point(lines_cluster[index], point);

      if (buffer > result)
        result = buffer;
    }

  return result;
}


// Результат классификации - массив массивов пар - (номер кадра, номер прямой на кадре)
std::vector<std::vector< std::pair<unsigned int, unsigned int> > > line_classifier_2(std::vector<shot_model> &model_shots)
{
  std::vector<std::vector< std::pair<unsigned int, unsigned int> > > result;
  std::vector< std::pair<unsigned int, unsigned int> > cluster_buffer;
  std::pair<unsigned int, unsigned int> line_buffer;
  std::vector< std::vector< std::pair <line_3d, bool> > > projection_lines_arrays;
  std::vector< std::pair <line_3d, bool> > *current_projection_lines;

  line_3d *lines_buffer;
  int size_lines_buffer;
  vector_3d point_buffer;

  unsigned int index, shots_iterator, current_shot, num_min, cluster_index, shot_index;
  bool round_flag;
  double min, min_buffer;

  // Конвертируем
  converter_shots_to_projection_lines_arrays(model_shots, projection_lines_arrays);

  // Кластерезуем
  for(shots_iterator = 0; shots_iterator < projection_lines_arrays.size(); ++shots_iterator)
    {
      current_projection_lines = &projection_lines_arrays[shots_iterator];
      // Обходим прямые на кадре
      for(index = 0; index < current_projection_lines->size(); ++index)
        {
          // Если прямая не обработана, возьмём её за основу нового кластера
          if(!projection_lines_arrays[shots_iterator][index].second)
            {
              line_buffer.first = shots_iterator;
              line_buffer.second = index;

              cluster_buffer.push_back(line_buffer);
              projection_lines_arrays[shots_iterator][index].second = true;

              // Обходим соседние кадры
              // Вперёд
              round_flag = true;
              current_shot = shots_iterator;

              while(round_flag)
                {
                  // Получим номер кадра
                  current_shot = get_next_shot(model_shots, current_shot);

                  // Найдём прямую, расстояние от которой, до нашей минимально

                  // Сконвертируем в массив кластер
                  size_lines_buffer = cluster_buffer.size() + 1;
                  lines_buffer = new line_3d[size_lines_buffer];

                  for(cluster_index = 0; cluster_index < cluster_buffer.size(); ++cluster_index)
                    lines_buffer[cluster_index] = projection_lines_arrays[cluster_buffer[cluster_index].first][cluster_buffer[cluster_index].second].first;

                  num_min = 0;
                  min = 100000;

                  for(shot_index = 0; shot_index < projection_lines_arrays[current_shot].size(); ++shot_index)
                    {
                      if(!projection_lines_arrays[current_shot][shot_index].second)
                        {
                          lines_buffer[cluster_index] = projection_lines_arrays[current_shot][shot_index].first;

                          // Нашли ближайшую точку
                          search_nearest_point_lines(lines_buffer, size_lines_buffer, point_buffer);

                          // Найдём расстояние от найденой точки до кластера
                          min_buffer = distanse_between_lines_cluster_and_points(lines_buffer, size_lines_buffer, point_buffer);

                          if(min_buffer < min)
                            {
                              min = min_buffer;
                              num_min = shot_index;
                            }
                        }
                    }

                  // Посмотрим, нашли ли прямую, при которой расстояние в кластере минимально и ниже порога
                  if(min < ASSUMPTIVE_ERROR)
                    {
                      line_buffer.first = current_shot;
                      line_buffer.second = num_min;

                      cluster_buffer.push_back(line_buffer);
                      projection_lines_arrays[current_shot][num_min].second = true;
                    }
                  else
                    {
                      round_flag = false;
                    }

                  delete[] lines_buffer;
                }

              // Назад
              round_flag = true;
              current_shot = shots_iterator;

              while(round_flag)
                {
                  // Получим номер кадра
                  current_shot = get_prev_shot(model_shots, current_shot);

                  // Найдём прямую, расстояние от которой, до нашей минимально

                  // Сконвертируем в массив кластер
                  size_lines_buffer = cluster_buffer.size() + 1;
                  lines_buffer = new line_3d[size_lines_buffer];

                  for(cluster_index = 0; cluster_index < cluster_buffer.size(); ++cluster_index)
                    lines_buffer[cluster_index] = projection_lines_arrays[cluster_buffer[cluster_index].first][cluster_buffer[cluster_index].second].first;

                  num_min = 0;
                  min = 100000;

                  for(shot_index = 0; shot_index < projection_lines_arrays[current_shot].size(); ++shot_index)
                    {
                      if(!projection_lines_arrays[current_shot][shot_index].second)
                        {
                          lines_buffer[cluster_index] = projection_lines_arrays[current_shot][shot_index].first;

                          // Нашли ближайшую точку
                          search_nearest_point_lines(lines_buffer, size_lines_buffer, point_buffer);

                          // Найдём расстояние от найденой точки до кластера
                          min_buffer = distanse_between_lines_cluster_and_points(lines_buffer, size_lines_buffer, point_buffer);

                          if(min_buffer < min)
                            {
                              min = min_buffer;
                              num_min = shot_index;
                            }
                        }
                    }

                  // Посмотрим, нашли ли прямую, при которой расстояние в кластере минимально и ниже порога
                  if(min < ASSUMPTIVE_ERROR)
                    {
                      line_buffer.first = current_shot;
                      line_buffer.second = num_min;

                      cluster_buffer.push_back(line_buffer);
                      projection_lines_arrays[current_shot][num_min].second = true;
                    }
                  else
                    {
                      round_flag = false;
                    }

                  delete[] lines_buffer;
                }
            }

          // Добавим новый кластер
          if(cluster_buffer.size() != 0)
            result.push_back(cluster_buffer);
          // Очистим буффер
          cluster_buffer.clear();
        }
    }

  return result;
}


// Поиск центра кластера
vector_3d search_cluster_center(std::vector<shot_model> &model_shots,
                           std::vector< std::pair<unsigned int, unsigned int> > &cluster)
{
  unsigned int index;
  line_3d buffer;
  vector_3d result;

  // Конвертируем кластер прямых в массив прямых
  line_3d *lines_buffer = new line_3d[cluster.size()];

  for(index = 0; index < cluster.size(); ++index)
    {
      buffer.directing_vector = model_shots[cluster[index].first].projection_vector;
      buffer.point = model_shots[cluster[index].first].vertexes[cluster[index].second];

      lines_buffer[index] = buffer;
    }

  search_nearest_point_lines(lines_buffer, cluster.size(), result);

  return result;
}

// Функция слияния двух кластеров. Второй кластер вливается в первый и удаляется
void merge_two_clusters(std::vector< std::pair<unsigned int, unsigned int> > &first_cluster,
                        std::vector< std::pair<unsigned int, unsigned int> > &second_cluster)
{
  unsigned int index;

  first_cluster.reserve(first_cluster.size() + second_cluster.size());

  for(index = 0; index < second_cluster.size(); ++index)
    first_cluster.push_back(second_cluster[index]);

  second_cluster.clear();
}

// Слияение кластеров
unsigned int merge_clusters(std::vector<shot_model> &model_shots,
                    std::vector<std::vector< std::pair<unsigned int, unsigned int> > > &clusters)
{
  unsigned int index_1, index_2, index_3;
  double distance;
  line_3d *lines_buffer;
  line_3d line_buffer;
  unsigned int merge_counter = 0;

  // Найдём центры кластеров
  vector_3d *centers_clusters = new vector_3d[clusters.size()];
  for(index_1 = 0; index_1 < clusters.size(); ++index_1)
    centers_clusters[index_1] = search_cluster_center(model_shots, clusters[index_1]);

  // Обход кластеров
  for(index_1 = 0; index_1 < clusters.size(); ++index_1)
    {
      if(clusters[index_1].size() < 2)
        continue;

      for(index_2 = index_1 + 1; index_2 < clusters.size(); ++index_2)
        {
          if(clusters[index_2].size() < 2)
            continue;

          distance = (centers_clusters[index_1] - centers_clusters[index_2]).length();
          if(distance >= DISTANCE_NEAR_CLUSTERS)
            continue;

          // Произведём слияние двух кластеров
          merge_two_clusters(clusters[index_1], clusters[index_2]);
          ++merge_counter;

          // Пересчитаем центр нового кластера
          lines_buffer = new line_3d[clusters[index_1].size()];

          for(index_3 = 0; index_3 < clusters[index_1].size(); ++index_3)
            {
              line_buffer.directing_vector = model_shots[clusters[index_1][index_3].first].projection_vector;
              line_buffer.point = model_shots[clusters[index_1][index_3].first].vertexes[clusters[index_1][index_3].second];

              lines_buffer[index_3] = line_buffer;
            }

          search_nearest_point_lines(lines_buffer, clusters[index_1].size(), centers_clusters[index_1]);

          delete[] lines_buffer;
        }
    }

  // Зачистим список кластеров от пустых кластеров
  std::vector<std::vector< std::pair<unsigned int, unsigned int> > >::iterator clusters_iterator;
  for(clusters_iterator = clusters.begin(); clusters_iterator != clusters.end(); ++clusters_iterator)
    {
      if(clusters_iterator->size() == 0)
        clusters_iterator = clusters.erase(clusters_iterator);
    }

  delete[] centers_clusters;

  return merge_counter;
}
