#ifndef MATH_FUNCTIONS_H
#define MATH_FUNCTIONS_H

#include <math.h>

#include "lib/const.h"

#include "lib/geometry_lib/vector_3d.h"

// Получение полярного угла
double get_polar_angle(const vector_3d &point);

// Умножение матрицы на вектор.
void multiplication_matrix_by_vector(double *matrix, int size_matrix, double *vector, double *result);

#endif // MATH_FUNCTIONS_H
