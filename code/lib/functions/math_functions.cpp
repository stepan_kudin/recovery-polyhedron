#include "math_functions.h"

// Получение полярного угла
double get_polar_angle(const vector_3d &point)
{
  if((point.x > EPS || point.x < -1.0 * EPS) && point.y > EPS)
    return atan2(point.y, point.x);
  if(fabs(point.x) > EPS && point.y < -1 * EPS)
    return atan2(point.y, point.x) + 2.0 * PI;
  if(fabs(point.x) < EPS && point.y > EPS)
    return PI / 2.0;
  if(fabs(point.x) < EPS && point.y < -1 * EPS)
    return 3.0 * PI / 2;
  if(fabs(point.x) < EPS && fabs(point.y) < EPS)
    return 0.0;
  if(fabs(point.y) < EPS && point.x > EPS)
    return 0.0;
  if(fabs(point.y) < EPS && point.x < -1.0 * EPS)
    return PI;

  return 0.0;
}

// Умножение матрицы на вектор.
void multiplication_matrix_by_vector(double *matrix, int size_matrix, double *vector, double *result)
{
  int index_1, index_2;
  double buffer;

  for(index_1 = 0; index_1 < size_matrix; ++index_1)
    {
      buffer = 0.0;
      for(index_2 = 0; index_2 < size_matrix; ++index_2)
        buffer += matrix[index_1 * size_matrix + index_2] * vector[index_2];
      result[index_1] = buffer;
    }
}
