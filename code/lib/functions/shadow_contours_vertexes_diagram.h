#ifndef SHADOW_CONTOURS_VERTEXES_DIAGRAM_H
#define SHADOW_CONTOURS_VERTEXES_DIAGRAM_H

#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <vector>

#include "lib/const.h"

#include "lib/structures/shot_model.h"
#include "lib/structures/diagram_point.h"

#include "lib/functions/math_functions.h"

// Построение диаграммы.
std::vector<std::vector<diagram_point>> create_shadow_contours_vertexes_diagram(std::vector<shot_model> &model_shots);

// Рисование диаграммы.
int draw_diagram(std::vector<std::vector<diagram_point>> &diagram, const char *filename);

// Компаратор.
bool compare_diagram_point(diagram_point point_1, diagram_point point_2);

#endif // SHADOW_CONTOURS_VERTEXES_DIAGRAM_H
