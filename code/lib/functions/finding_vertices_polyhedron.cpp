#include "finding_vertices_polyhedron.h"

int lines_intersection(vector_3d line_1_point, vector_3d line_1_vector,
                       vector_3d line_2_point, vector_3d line_2_vector, vector_3d &result)
{
  double matrix[4];
  double right_hand_member[2];
  double res[2];

  // Вторая точка на прямой.
  vector_2d b;

  int ret;

  b.x = line_1_point.x + line_1_vector.x;
  b.y = line_1_point.y + line_1_vector.y;

  matrix[0] = line_1_point.y - b.y;
  matrix[1] = b.x - line_1_point.x;
  right_hand_member[0] = line_1_point.x * b.y - line_1_point.y * b.x;

  b.x = line_2_point.x + line_2_vector.x;
  b.y = line_2_point.y + line_2_vector.y;

  matrix[2] = line_2_point.y - b.y;
  matrix[3] = b.x - line_2_point.x;
  right_hand_member[1] = line_2_point.x * b.y - line_2_point.y * b.x;

  ret = reflections_method_solver(&matrix[0], 2, &right_hand_member[0], &res[0]);

  if(ret)
    return ret;

  result.x = res[0];
  result.y = res[1];
  result.z = (line_1_point.z + line_2_point.z) / 2;

  return 0;
}

std::list<vertex_cluster> find_vertices_in_cluster(std::vector<frame_point> &cluster, std::vector<shot_model> &shots)
{
  std::list<vertex_cluster> result;

  // Построим попарные пересечения прямых.
  std::list<point_for_forel> points;
  unsigned int index;
  unsigned int size;
  vector_3d point_buffer;

  FILE *out = fopen("points.txt", "w");

  size = cluster.size() - 1;
  for(index = 0; index < size; ++index)
    {
      if(lines_intersection(shots[cluster[index].frame_index].vertexes[cluster[index].vertex_index],
                            shots[cluster[index].frame_index].projection_vector,
                            shots[cluster[index + 1].frame_index].vertexes[cluster[index + 1].vertex_index],
                            shots[cluster[index + 1].frame_index].projection_vector,
                            point_buffer))
        {
          printf("Wrong line intersection!\n");
          abort();
        }

      points.emplace_back(point_buffer, cluster[index], cluster[index + 1]);

      fprintf(out, "%lf %lf %lf\n", point_buffer.x, point_buffer.y, point_buffer.z);
    }

  fclose(out);

  // Кластеризуем точки алгоритмом ФорЭл.
  vertex_cluster vertex;
  vector_3d new_center;

  while(points.size() > 0)
    {
      new_center = points.front().point;

      do
        {
          vertex.points.clear();
          vertex.center = new_center;
          // Обойдём точки, добавим в список все, расстояние до которых меньше порога.
          for(auto it : points)
            {
              if((vertex.center - it.point).length() < FOREL_CRITERION + EPS)
                vertex.points.push_back(it);
            }

          // Посчитаем новый центр кластера.
          new_center.x = 0.0;
          new_center.y = 0.0;
          new_center.z = 0.0;

          for(auto it : vertex.points)
            {
              new_center.x += it.point.x;
              new_center.y += it.point.y;
              new_center.z += it.point.z;
            }

          new_center /= vertex.points.size();
        } while(new_center != vertex.center);

      // Добавим новый кластер.
      result.push_back(vertex);

      // Удалим точки, которые уже кластеризованы.
      auto del_point = vertex.points.begin();
      for(auto it = points.begin(); it != points.end(); ++it)
        {
          if((*it).point == (*del_point).point)
            {
              it = points.erase(it);
              --it;
              ++del_point;

              if(del_point == vertex.points.end())
                break;
            }
        }
    }

  return result;
}

void delete_noise(std::list<vertex_cluster> &clusters)
{
  for(auto it = clusters.begin(); it != clusters.end();)
    {
      if((*it).points.size() == 1)
        {
          it = clusters.erase(it);
          continue;
        }
      ++it;
    }
}

void merge_vertices(std::list<vertex_cluster> &clusters)
{
  unsigned int size_1, size_2;

  for(auto cluster = clusters.begin(); cluster != clusters.end(); ++cluster)
    {
      auto it = cluster;
      for(++it; it != clusters.end();)
        {
          if(((*cluster).center - (*it).center).length() < FOREL_CRITERION + EPS)
            {
              size_1 = (*cluster).points.size();
              size_2 = (*it).points.size();

              (*cluster).points.insert((*cluster).points.end(), (*it).points.begin(), (*it).points.end());

              (*cluster).center = (size_1 * (*cluster).center + size_2 * (*it).center) / (size_1 + size_2);

              it = clusters.erase(it);

              continue;
            }
          ++it;
        }
    }


}

std::list<vertex_cluster> find_vertices(std::list<std::vector<frame_point>> &clusters, std::vector<shot_model> &shots)
{
  std::list<vertex_cluster> result;
  std::list<vertex_cluster> buffer;

  for(auto it : clusters)
    {
      buffer = find_vertices_in_cluster(it, shots);

      delete_noise(buffer);

      result.splice(result.end(), buffer);

      buffer.clear();
    }

  merge_vertices(result);

  return result;
}
