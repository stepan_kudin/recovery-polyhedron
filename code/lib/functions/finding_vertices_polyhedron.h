#ifndef FINDING_VERTICES_POLYHEDRON_H
#define FINDING_VERTICES_POLYHEDRON_H

#include <stdio.h>
#include <math.h>

#include <vector>
#include <list>

#include "lib/const.h"

#include "lib/geometry_lib/vector_2d.h"
#include "lib/geometry_lib/vector_3d.h"

#include "lib/linear_system_solver/reflections_method.h"

#include "lib/structures/shot_model.h"
#include "lib/structures/frame_point.h"
#include "lib/structures/point_for_forel.h"
#include "lib/structures/vertex_cluster.h"

int lines_intersection(vector_3d line_1_point, vector_3d line_1_vector,
                       vector_3d line_2_point, vector_3d line_2_vector, vector_3d &result);

std::list<vertex_cluster> find_vertices_in_cluster(std::vector<frame_point> &cluster, std::vector<shot_model> &shots);

void delete_noise(std::list<vertex_cluster> &clusters);

void merge_vertices(std::list<vertex_cluster> &clusters);

std::list<vertex_cluster> find_vertices(std::list<std::vector<frame_point>> &clusters, std::vector<shot_model> &shots);

#endif // FINDING_VERTICES_POLYHEDRON_H
