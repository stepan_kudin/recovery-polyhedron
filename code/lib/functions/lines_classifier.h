#ifndef LINES_CLASSIFIER_H
#define LINES_CLASSIFIER_H

#include <iostream>
#include <stdio.h>
#include <math.h>

#include <vector>
#include <stack>
#include <list>

#include "../geometry_lib/vector_3d.h"
#include "../geometry_lib/line_3d.h"

#include "../linear_system_solver/reflections_method.h"

#include "../structures/shot_model.h"

#include "../const.h"

double distance_between_line_and_point (line_3d line, vector_3d point);
int search_nearest_point_lines(line_3d *lines, int count, vector_3d &result);
std::vector< std::list<int> > lines_classifier(line_3d *lines, int lines_size);

unsigned int get_next_shot(std::vector<shot_model> &model_shots, unsigned int index);
unsigned int get_prev_shot(std::vector<shot_model> &model_shots, unsigned int index);
void converter_shots_to_projection_lines_arrays(std::vector<shot_model> &model_shots,
                                                std::vector< std::vector< std::pair <line_3d, bool> > > &projection_lines_arrays);
double distanse_between_lines_cluster_and_points(line_3d *lines_cluster, int size_lines_cluster, vector_3d point);
// Результат классификации - массив массивов пар - (номер кадра, номер прямой на кадре)
std::vector<std::vector< std::pair<unsigned int, unsigned int> > > line_classifier_2(std::vector<shot_model> &model_shots);

// Поиск центра кластера
vector_3d search_cluster_center(std::vector<shot_model> &model_shots,
                           std::vector< std::pair<unsigned int, unsigned int> > &cluster);

// Функция слияния двух кластеров. Второй кластер вливается в первый и удаляется
void merge_two_clusters(std::vector< std::pair<unsigned int, unsigned int> > &first_cluster,
                        std::vector< std::pair<unsigned int, unsigned int> > &second_cluster);

// Слияение кластеров
unsigned int merge_clusters(std::vector<shot_model> &model_shots,
                    std::vector<std::vector< std::pair<unsigned int, unsigned int> > > &clusters);

#endif // LINES_CLASSIFIER_H
