#include "shadow_contours_vertexes_diagram.h"

// Построение диаграмы
std::vector<std::vector<diagram_point>> create_shadow_contours_vertexes_diagram(std::vector<shot_model> &model_shots)
{
  unsigned int index_1, index_2;
  std::vector<std::vector<diagram_point>> result;
  std::vector<diagram_point> frame_buffer;
  diagram_point buffer;

  // Заполним вектор точек вершинами
  for(index_1 = 0; index_1 < model_shots.size(); ++index_1)
    {
      for(index_2 = 0; index_2 < model_shots[index_1].vertexes.size(); ++index_2)
        {
          buffer.point.x = get_polar_angle(model_shots[index_1].projection_vector);
          buffer.point.y = model_shots[index_1].vertexes[index_2].z;
          buffer.frame_index = index_1;
          buffer.vertex_index = index_2;

          frame_buffer.push_back(buffer);
        }
      sort(frame_buffer.begin(), frame_buffer.end(), compare_diagram_point);

      result.push_back(frame_buffer);

      frame_buffer.clear();
    }

  return result;
}

int draw_diagram(std::vector<std::vector<diagram_point>> &diagram, const char *filename)
{
  // Запишем результат в файл
  FILE *out = fopen(filename, "w");

  if(out == NULL)
    return -1;

  fprintf(out, "ListPlot[{");

  unsigned int index_1, index_2;
  unsigned int frames_size = diagram.size() - 1;
  unsigned int point_in_frame;
  for(index_1 = 0; index_1 < frames_size; ++index_1)
  {
      point_in_frame = diagram[index_1].size();
      for(index_2 = 0; index_2 < point_in_frame; ++index_2)
        fprintf(out, "{%lf, %lf},", diagram[index_1][index_2].point.x, diagram[index_1][index_2].point.y);
    }

  point_in_frame = diagram[index_1].size() - 1;
  for(index_2 = 0; index_2 < point_in_frame; ++index_2)
    fprintf(out, "{%lf, %lf},", diagram[index_1][index_2].point.x, diagram[index_1][index_2].point.y);
  fprintf(out, "{%lf, %lf}", diagram[index_1][index_2].point.x, diagram[index_1][index_2].point.y);

  fprintf(out, "}]");

  fclose(out);

  return 0;
}

bool compare_diagram_point(diagram_point point_1, diagram_point point_2)
{
  return point_1.point.y < point_2.point.y;
}
