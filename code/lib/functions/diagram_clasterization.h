#ifndef DIAGRAM_CLASTERIZATION_H
#define DIAGRAM_CLASTERIZATION_H

#include <stdio.h>
#include <math.h>

#include <algorithm>
#include <vector>
#include <list>
#include <queue>

#include "lib/const.h"

#include "lib/geometry_lib/vector_2d.h"
#include "lib/geometry_lib/vector_3d.h"

#include "lib/structures/diagram_point.h"
#include "lib/structures/frame_point.h"
#include "lib/structures/shot_model.h"

#include "lib/functions/math_functions.h"
#include "lib/functions/shadow_contours_vertexes_diagram.h"

#include "lib/linear_system_solver/reflections_method.h"


enum class frame_vertex_t
{
  UNDEFINED,
  RIGHT,
  LINE,
  LEFT
};

std::vector<std::vector<frame_vertex_t>> frame_vertex_marking(std::vector<std::vector<diagram_point>> &diagram,
                                                              std::vector<shot_model> &shots);

vector_2d get_local_coord(const vector_3d &point, double *matrix, const vector_3d &center);

void print_frame_vertex_marks(std::vector<std::vector<frame_vertex_t>> &frame_vertex_marks);

std::list<std::vector<frame_point>> diagram_clusterization(std::vector<std::vector<diagram_point>> &diagram,
                                                           std::vector<shot_model> &shots,
                                                           std::vector<std::vector<frame_vertex_t> > frame_vertex_marks);

int draw_diagram_clusterization(std::list<std::vector<frame_point>> &diagram_clusters,
                                std::vector<shot_model> &shots,
                                const char *filename);

void delete_noise(std::list<std::vector<frame_point>> &diagram_clusters, unsigned int &count_deleted_clusters);

#endif // DIAGRAM_CLASTERIZATION_H
