#include "diagram_clasterization.h"

std::vector<std::vector<frame_vertex_t>> frame_vertex_marking(std::vector<std::vector<diagram_point>> &diagram,
                                                              std::vector<shot_model> &shots)
{
  std::vector<std::vector<frame_vertex_t>> result;
  unsigned int index_1, index_2;
  double transition_matrix[9];
  double inverse_transition_matrix[9];
  vector_3d max_1, max_2;
  vector_3d min_1, min_2;
  vector_3d proj_vector;
  vector_3d max, min;
  double d;
  vector_2d line, point;

  for(index_1 = 0; index_1 < diagram.size(); ++index_1)
    result.emplace_back(diagram[index_1].size(), frame_vertex_t::UNDEFINED);

  for(index_1 = 0; index_1 < diagram.size(); ++index_1)
    {
      // Найдём максимум и минимум на кадре.
      min_1 = shots[index_1].vertexes[diagram[index_1][0].vertex_index];
      min_2 = shots[index_1].vertexes[diagram[index_1][1].vertex_index];

      if(fabs(min_1.z - min_2.z) < 2 * SCAN_ERROR + EPS)
        {
          min.x = (min_1.x + min_2.x) / 2.0;
          min.y = (min_1.y + min_2.y) / 2.0;
          min.z = (min_1.z + min_2.z) / 2.0;
        }
      else
        {
          min = min_1;
          result[index_1][diagram[index_1][0].vertex_index] = frame_vertex_t::LINE;
        }

      max_1 = shots[index_1].vertexes[diagram[index_1][diagram[index_1].size() - 1].vertex_index];
      max_2 = shots[index_1].vertexes[diagram[index_1][diagram[index_1].size() - 2].vertex_index];

      if(fabs(max_1.z - max_2.z) < 2 * SCAN_ERROR + EPS)
        {
          max.x = (max_1.x + max_2.x) / 2.0;
          max.y = (max_1.y + max_2.y) / 2.0;
          max.z = (max_1.z + max_2.z) / 2.0;
        }
      else
        {
          max = max_1;
          result[index_1][diagram[index_1][diagram[index_1].size() - 1].vertex_index] = frame_vertex_t::LINE;
        }

      // Матрица перехода.
      proj_vector = shots[index_1].projection_vector;
      proj_vector.norm();

      transition_matrix[0] = (-1.0) * proj_vector.y;
      transition_matrix[1] = 0.0;
      transition_matrix[2] = proj_vector.x;
      transition_matrix[3] = proj_vector.x;
      transition_matrix[4] = 0.0;
      transition_matrix[5] = proj_vector.y;
      transition_matrix[6] = 0.0;
      transition_matrix[7] = 1.0;
      transition_matrix[8] = 0.0;

      // Найдём обратную к ней матрицу.
      if(reflections_method_inverse_matrix(&transition_matrix[0], 3, &inverse_transition_matrix[0]))
        abort();

      // Найдём коэффициент d плоскости. Поскольку вектор нормированный, то
      // расстояние от центра системы координат до плоскости проекции равно этому числу.
      d = (-1.0) * proj_vector * min;

      vector_3d local_coord_center(d * proj_vector.x, d * proj_vector.y, 0);

      // Зададим вектор рассекающей прямой.
      line = get_local_coord(max, inverse_transition_matrix, local_coord_center) -
          get_local_coord(min, inverse_transition_matrix, local_coord_center);

      // Обойдём все вершины на кадре и выведем их тип.
      for(index_2 = 0; index_2 < diagram[index_1].size(); ++index_2)
        {
          if(result[index_1][diagram[index_1][index_2].vertex_index] == frame_vertex_t::UNDEFINED)
            {
              point = get_local_coord(shots[index_1].vertexes[diagram[index_1][index_2].vertex_index],
                  inverse_transition_matrix, local_coord_center);

              if((line % point) > 0.0)
                result[index_1][diagram[index_1][index_2].vertex_index] = frame_vertex_t::LEFT;
              else
                result[index_1][diagram[index_1][index_2].vertex_index] = frame_vertex_t::RIGHT;
            }
        }
    }

  return result;
}

vector_2d get_local_coord(const vector_3d &point, double *matrix, const vector_3d &center)
{
  double vector[3];
  double result[3];

  vector[0] = point.x - center.x;
  vector[1] = point.y - center.y;
  vector[2] = point.z;

  multiplication_matrix_by_vector(matrix, 3, &vector[0], &result[0]);

  return vector_2d(result[0], result[1]);
}

void print_frame_vertex_marks(std::vector<std::vector<frame_vertex_t>> &frame_vertex_marks)
{
  unsigned int index_1, index_2;

  FILE *out = fopen("frame_vertex_marks.txt", "w");

  if(out == NULL)
    return;

  for(index_1 = 0; index_1 < frame_vertex_marks.size(); ++index_1)
    {
      fprintf(out, "-------------------- %d frame ------------------------\n", index_1);
      for(index_2 = 0; index_2 < frame_vertex_marks[index_1].size(); ++index_2)
        {
          fprintf(out, "vertex index: %d, ", index_2);

          switch(frame_vertex_marks[index_1][index_2])
            {
            case frame_vertex_t::UNDEFINED:
              fprintf(out, "mark: UNDEFINED\n");
              break;
            case frame_vertex_t::LEFT:
              fprintf(out, "mark: LEFT\n");
              break;
            case frame_vertex_t::LINE:
              fprintf(out, "mark: LINE\n");
              break;
            case frame_vertex_t::RIGHT:
              fprintf(out, "mark: RIGHT\n");
              break;
            }
        }
    }

  fclose(out);
}

std::list<std::vector<frame_point> > diagram_clusterization(std::vector<std::vector<diagram_point>> &diagram,
                                                           std::vector<shot_model> &shots,
                                                           std::vector<std::vector<frame_vertex_t>> frame_vertex_marks)
{
  std::list<std::vector<frame_point>> result;
  std::vector<std::vector<bool>> no_clustered;
  unsigned int index_1, index_2, index_3;
  std::vector<frame_point> cluster;
  std::vector<diagram_point>::iterator current;
  double max;
  diagram_point min;
  frame_vertex_t vertex_type;
  bool add_flag;

  for(index_1 = 0; index_1 < diagram.size(); ++index_1)
    no_clustered.emplace_back(diagram[index_1].size(), true);

  for(index_1 = 0; index_1 < shots.size(); ++index_1)
    {
      for(index_2 = 0; index_2 < shots[index_1].vertexes.size(); ++index_2)
        {
          if(no_clustered[index_1][index_2])
            {
              // Основа нового кластера.
              cluster.emplace_back(index_1, index_2);

              max = shots[index_1].vertexes[index_2].z + TAU;
              min.point.y = shots[index_1].vertexes[index_2].z - TAU;

              vertex_type = frame_vertex_marks[index_1][index_2];

              no_clustered[index_1][index_2] = false;

              // Пройдёмся по следующим кадрам.
              for(index_3 = index_1 + 1; index_3 < diagram.size(); ++index_3)
                {
                  // Найдём точку с наибольшим z, который меньше min.
                  current = std::lower_bound(diagram[index_3].begin(), diagram[index_3].end(), min, compare_diagram_point);

                  // Теперь движемся вверх и останавливаемся, когда найдём точку нужного класса в нужном отрезке.                    
                  add_flag = false;
                  if(current != diagram[index_3].end())
                    {
                      while((*current).point.y < max + EPS)
                        {
                          if(frame_vertex_marks[(*current).frame_index][(*current).vertex_index] == vertex_type)
                            {
                              add_flag = true;
                              break;
                            }

                          ++current;
                        }
                    }

                  if(add_flag)
                    {
                      // Добавляем точку в кластер.
                      cluster.emplace_back((*current).frame_index, (*current).vertex_index);
                      no_clustered[(*current).frame_index][(*current).vertex_index] = false;
                    }
                  else
                    break;
                }

              result.push_back(cluster);
              cluster.clear();
            }
        }
    }

  return result;
}

int draw_diagram_clusterization(std::list<std::vector<frame_point> > &diagram_clusters,
                                std::vector<shot_model> &shots,
                                const char *filename)
{
  FILE *out = fopen(filename, "w");
  if(out == NULL)
    return -1;

  unsigned int index, size_cluster;
  vector_3d buffer;
  std::list<std::vector<frame_point>>::iterator list_it, max_it;

  max_it = diagram_clusters.end();
  --max_it;

  fprintf(out, "ListPlot[{");

  for(list_it = diagram_clusters.begin(); list_it != max_it; ++list_it)
    {
      fprintf(out, "{");

      size_cluster = (*list_it).size() - 1;

      for(index = 0; index < size_cluster; ++index)
        {
          buffer = shots[(*list_it)[index].frame_index].vertexes[(*list_it)[index].vertex_index];
          fprintf(out, "{%lf, %lf},", get_polar_angle(shots[(*list_it)[index].frame_index].projection_vector),
              buffer.z);
        }

      buffer = shots[(*list_it)[index].frame_index].vertexes[(*list_it)[index].vertex_index];
      fprintf(out, "{%lf, %lf}}, ", get_polar_angle(shots[(*list_it)[index].frame_index].projection_vector),
          buffer.z);
    }

  fprintf(out, "{");

  size_cluster = (*list_it).size() - 1;

  for(index = 0; index < size_cluster; ++index)
    {
      buffer = shots[(*list_it)[index].frame_index].vertexes[(*list_it)[index].vertex_index];
      fprintf(out, "{%lf, %lf},", get_polar_angle(shots[(*list_it)[index].frame_index].projection_vector),
          buffer.z);
    }

  buffer = shots[(*list_it)[index].frame_index].vertexes[(*list_it)[index].vertex_index];
  fprintf(out, "{%lf, %lf}}}, ", get_polar_angle(shots[(*list_it)[index].frame_index].projection_vector),
      buffer.z);

  // Print legend.
  fprintf(out, "PlotLegends -> {");

  size_cluster = diagram_clusters.size() - 1;
  for(index = 0; index < size_cluster; ++index)
    fprintf(out, "\"%d cluster\", ", index + 1);

  fprintf(out, "\"%d cluster\"}]", index + 1);

  fclose(out);

  return 0;
}

void delete_noise(std::list<std::vector<frame_point>> &diagram_clusters, unsigned int &count_deleted_clusters)
{
  count_deleted_clusters = 0;
  for(auto it = diagram_clusters.begin(); it != diagram_clusters.end(); ++it)
    {
      if((*it).size() <= CRITERION_NOISE)
        {
          it = diagram_clusters.erase(it);
          --it;
          ++count_deleted_clusters;
        }
    }
}
