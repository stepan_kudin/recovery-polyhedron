#ifndef ORIGINAL_MODEL_READER_H
#define ORIGINAL_MODEL_READER_H

#include <stdio.h>
#include <vector>

#include "../geometry_lib/vector_3d.h"

int original_model_vertexes_reader(const char *filename, std::vector<vector_3d> &vertexes);

#endif // ORIGINAL_MODEL_READER_H
