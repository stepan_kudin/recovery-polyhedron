#ifndef SHADOW_CONTOURS_READER_H
#define SHADOW_CONTOURS_READER_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <vector>

#include "../const.h"

#include "../geometry_lib/vector_3d.h"
#include "../geometry_lib/line_3d.h"

#include "../structures/shot_model.h"

line_3d *shadow_contours_reader_line(const char *filename, unsigned int &lines_size);
int check_duplicate_point(std::vector<vector_3d> points, vector_3d &point);
int read_shot_model(const char *filename, std::vector<shot_model> &shots);

#endif // SHADOW_CONTOURS_READER_H
