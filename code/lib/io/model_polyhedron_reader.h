#ifndef MODEL_POLYHEDRON_READER_H
#define MODEL_POLYHEDRON_READER_H

#include <stdio.h>

#include <vector>

#include "../geometry_lib/vector_3d.h"

vector_3d *model_polyhedron_reader_point(const char *filename, unsigned int &result_size);

#endif // MODEL_POLYHEDRON_READER_H
