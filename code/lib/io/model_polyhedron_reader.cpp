#include "model_polyhedron_reader.h"

vector_3d *model_polyhedron_reader_point(const char *filename, unsigned int &result_size)
{
  unsigned int count_vertex, buffer, index;
  vector_3d point_buffer;
  std::vector<vector_3d> points_buffer;

  FILE *in = fopen(filename, "r");
  if(in == NULL)
    return 0;

  // Прочитаем количество вершин
  fscanf(in, "%d", &count_vertex);

  // Прочитам количество граней
  fscanf(in, "%d", &buffer);

  // Прочитаем количество вершин
  fscanf(in, "%d", &buffer);

  for(index = 0; index < count_vertex; ++index)
    {
      fscanf(in, "%d", &buffer);

      fscanf(in, "%lf %lf %lf", &point_buffer.x, &point_buffer.y, &point_buffer.z);
      points_buffer.push_back(point_buffer);
    }

  vector_3d *vertices = new vector_3d[count_vertex];
  result_size = count_vertex;

  for(index = 0; index < points_buffer.size(); ++index)
      vertices[index] = points_buffer[index];

  fclose(in);

  return vertices;
}
