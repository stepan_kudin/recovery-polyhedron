#include "original_model_reader.h"

int original_model_vertexes_reader(const char *filename, std::vector<vector_3d> &vertexes)
{
  unsigned int index, size_vertexes;
  int buffer;

  FILE *in = fopen(filename, "r");
  if(in == NULL)
    return -1;

  // Прочитаем количество вершин
  fscanf(in, "%d %d %d", &size_vertexes, &buffer, &buffer);

  // Прочитаем массив вершин
  vector_3d point;
  for(index = 0; index < size_vertexes; ++index)
    {
      fscanf(in, "%d %lf %lf %lf", &buffer, &point.x, &point.y, &point.z);
      vertexes.push_back(point);
    }

  fclose(in);

  return 0;
}
