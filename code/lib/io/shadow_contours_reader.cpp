#include "shadow_contours_reader.h"

line_3d *shadow_contours_reader_line(const char *filename, unsigned int &lines_size)
{
  unsigned int contours_count, contours_num, count_edges, type_edge;
  double reliability;
  unsigned int index_1, index_2;
  std::vector<line_3d> lines_buffer;
  line_3d buffer_line;
  vector_3d normal_vector, point_buffer;
  std::vector<vector_3d> points_buffer;

  lines_size = 0;
  FILE *in = fopen(filename, "r");
  if(in == NULL)
    return 0;

  // Считаем количество контуров
  fscanf(in, "%d", &contours_count);

  // Считаем контуры
  for(index_1 = 0; index_1 < contours_count; ++index_1)
    {
      // Номер контура
      fscanf(in, "%d", &contours_num);

      // Вектор проектирования
      fscanf(in, "%lf %lf %lf", &normal_vector.x, &normal_vector.y, &normal_vector.z);

      // Количество рёбер в контуре
      fscanf(in, "%d", &count_edges);

      for(index_2 = 0; index_2 < count_edges; ++index_2)
        {
          // Тип ребра и достоверность
          fscanf(in, "%d %lf", &type_edge, &reliability);

          // Читаем начало ребра
          fscanf(in, "%lf %lf %lf", &point_buffer.x, &point_buffer.y, &point_buffer.z);

          // Выясним, дубль ли?
          if(!check_duplicate_point(points_buffer, point_buffer))
            {
              // Если не дубль, добавим
              points_buffer.push_back(point_buffer);
            }

          // Читаем конец ребра
          fscanf(in, "%lf %lf %lf", &point_buffer.x, &point_buffer.y, &point_buffer.z);

          // Выясним, дубль ли?
          if(!check_duplicate_point(points_buffer, point_buffer))
            {
              // Если не дубль, добавим
              points_buffer.push_back(point_buffer);
            }
        }

      // Добавим в буффер новые прямые
      for(index_2 = 0; index_2 < points_buffer.size(); ++index_2)
        {
          buffer_line.point = points_buffer[index_2];
          buffer_line.directing_vector = normal_vector;

          lines_buffer.push_back(buffer_line);
        }

      // Очистим буффер точек
      points_buffer.clear();
    }

  // Переконвертируем массив точек
  lines_size = lines_buffer.size();
  line_3d *lines = new line_3d[lines_size];

  for(index_1 = 0; index_1 < lines_size; ++index_1)
    {
      lines[index_1] = lines_buffer[index_1];
    }

  fclose(in);
  return lines;
}

int check_duplicate_point(std::vector<vector_3d> points, vector_3d &point)
{
  unsigned int index;
  double norm;

  for(index = 0; index < points.size(); ++index)
    {
      norm = sqrt((point.x - points[index].x) * (point.x - points[index].x) +
                  (point.y - points[index].y) * (point.y - points[index].y) +
                  (point.z - points[index].z) * (point.z - points[index].z));

      if(norm < SENSITIVITY_DUPLICATES)
        return 1;
    }

  return 0;
}

int read_shot_model(const char *filename, std::vector<shot_model> &shots)
{
  unsigned int contours_count;
  unsigned int edges_count;
  unsigned int index_1, index_2;
  int int_buffer;
  double double_buffer;
  shot_model shot_buffer;
  vector_3d vertex_1, vertex_2;
  int index_vertex_1, index_vertex_2;
  std::pair<int, int> edge_buffer;

  shot_buffer.vertexes.clear();
  shot_buffer.edges.clear();

  // Откроем файл
  FILE *in = fopen(filename, "r");

  if(in == NULL)
    return -1;

  // Прочитаем количество контуров
  fscanf(in, "%d", &contours_count);

  // Считаем контуры
  for(index_1 = 0; index_1 < contours_count; ++index_1)
    {
      // Вектор проекции
      fscanf(in, "%d %lf %lf %lf", &int_buffer, &shot_buffer.projection_vector.x, &shot_buffer.projection_vector.y,
             &shot_buffer.projection_vector.z);

      // Количество рёбер
      fscanf(in, "%d", &edges_count);

      // Считаем вершины и рёбра
      for(index_2 = 0; index_2 < edges_count; ++index_2)
        {
          // Тип ребра и достоверность (пропускаются)
          fscanf(in, "%d %lf", &int_buffer, &double_buffer);

          // Считаем вершины
          fscanf(in, "%lf %lf %lf %lf %lf %lf", &vertex_1.x, &vertex_1.y, &vertex_1.z,
                 &vertex_2.x, &vertex_2.y, &vertex_2.z);

          // Поиск вхождения вершин в список вершин и получение их индекса
          index_vertex_1 = shot_buffer.get_index_point(vertex_1);

          if(index_vertex_1 == -1)
            {
              shot_buffer.vertexes.push_back(vertex_1);
              index_vertex_1 = shot_buffer.vertexes.size() - 1;
            }

          index_vertex_2 = shot_buffer.get_index_point(vertex_2);

          if(index_vertex_2 == -1)
            {
              shot_buffer.vertexes.push_back(vertex_2);
              index_vertex_2 = shot_buffer.vertexes.size() - 1;
            }

          // Добавим ребро в кадр
          edge_buffer.first = index_vertex_1;
          edge_buffer.second = index_vertex_2;
          shot_buffer.edges.push_back(edge_buffer);
        }

      // Положим результат в список
      shots.push_back(shot_buffer);

      // Очистка буффера
      shot_buffer.vertexes.clear();
      shot_buffer.edges.clear();
    }

  fclose(in);

  return 0;
}
