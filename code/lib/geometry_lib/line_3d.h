#ifndef LINE_3D_H
#define LINE_3D_H

#include <iostream>
#include "vector_3d.h"

class line_3d
{
public:
  line_3d(vector_3d start_point, vector_3d dir_vector)
  {
    dir_vector.norm();
    point = start_point;
    directing_vector = dir_vector;
  }

  line_3d()
  {
    point.null();
    directing_vector.null();
  }

  ~line_3d() {}

  vector_3d point;
  vector_3d directing_vector;
};

#endif // LINE_3D_H
