#ifndef POLYGON_3D_H
#define POLYGON_3D_H

#include "gl.h"
#include "vector_3d.h"

class polygon_3d
{
public:
  polygon_3d()
  {
    vertexes = 0;
    size = 0;
    type = gl::INDEFINITE;

    a = 0.0;
    b = 0.0;
    c = 0.0;
    d = 0.0;
  }

  // Список вершин, количество вершин, тип многоугольника
  vector_3d *vertexes;
  int size;
  gl::polygon_type type;

  // Коэффиценты канонического уравнени плоскости, на которой лежит многоугольник
  double a, b, c, d;
};

#endif // POLYGON_3D_H
