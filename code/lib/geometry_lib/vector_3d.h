#ifndef VECTOR_3D_H
#define VECTOR_3D_H

#include "gl.h"
#include "math.h"

class vector_3d
{
public:
  // Координаты вектора
  double x, y, z;

  // Конструкторы
  vector_3d() {}
  vector_3d(double a, double b, double c) : x(a), y(b), z(c) {}
  // Деструктор
  ~vector_3d() {}

  // Перегрузка операторов

  // Унарные операции
  // Унарное сложение вектора
  vector_3d &operator +=(const vector_3d &vector)
  {
    x += vector.x;
    y += vector.y;
    z += vector.z;
    return *this;
  }

  // Унарное вычитание векторов
  vector_3d &operator -=(const vector_3d &vector)
  {
    x -= vector.x;
    y -= vector.y;
    z -= vector.z;
    return *this;
  }

  // Унарное умножение вектора на вещественное число
  vector_3d &operator *=(const double &number)
  {
    x *= number;
    y *= number;
    z *= number;
    return *this;
  }

  // Унарное деление на вещественное число. Нет проверки деления на 0!
  vector_3d &operator /=(const double &number)
  {
    x /= number;
    y /= number;
    z /= number;
    return *this;
  }

  // Проверка вектора на равенство нулу. true, если координаты (0, 0)
  bool operator !() const
  {
    return fabs(x) < gl::EPS && fabs(y) < gl::EPS && fabs(z) < gl::EPS;
  }

  // Унарное умножение вектора на (-1)
  vector_3d operator -() const
  {
    return vector_3d (-x, -y, -z);
  }

  // Бинарные операции

  // Бинарное сложение
  friend inline const vector_3d operator +(const vector_3d &left, const vector_3d &right);
  // Бинарное вычитание
  friend inline const vector_3d operator -(const vector_3d &left, const vector_3d &right);
  // Умножение вектора на число справа
  friend inline const vector_3d operator *(const vector_3d &vector, const double &number);
  // Умножение на число слева
  friend inline const vector_3d operator *(const double &number, const vector_3d &vector);
  // Деление вектора на число. Проверка деления на 0 не производится!
  friend inline const vector_3d operator /(const vector_3d &vector, const double &number);
  // Скалярное произведение двух векторов
  friend inline double operator *(const vector_3d &left, const vector_3d &right);
  // Векторное произведение
  friend inline vector_3d operator %(const vector_3d &left, const vector_3d &right);
  // Операция неравно для векторов. true, если векторы не равны. Сравнение идёт без заданной точности.
  friend inline bool operator !=(const vector_3d &left, const vector_3d &right);
  // Операция проверки на равенство двух векторов. Сравнение абсолютное.
  friend inline bool operator ==(const vector_3d &left, const vector_3d &right);

  // Методы

  // Обнуление вектора
  void null()
  {
    x = 0.0;
    y = 0.0;
    z = 0.0;
  }
/*
  // Нахождение левого перпендикуляра к вектору
  vector_3d left_perpendicular () const
  {
    return vector_2d (-y, x);
  }

  // Нахождение правого перпендикуляра к вектору
  vector_2d right_perpendicular () const
  {
    return vector_2d (y, -x);
  }
*/
  // Норма вектора
  double length() const
  {
    return sqrt(x * x + y * y + z * z);
  }

  // Манхэттенская норма
  double manhattan_norm() const
  {
    return x * x + y * y + z * z;
  }

  // Нормирование вектора
  void norm()
  {
    double norm = this->length ();
    x /= norm;
    y /= norm;
    z /= norm;
  }
};

// Бинарные операторы

// Бинарное сложение
inline const vector_3d operator +(const vector_3d &left, const vector_3d &right)
{
  return vector_3d(left.x + right.x, left.y + right.y, left.z + right.z);
}

// Бинарное вычитание
inline const vector_3d operator -(const vector_3d &left, const vector_3d &right)
{
  return vector_3d(left.x - right.x, left.y - right.y, left.z - right.z);
}

// Умножение вектора на число справа
inline const vector_3d operator *(const vector_3d &vector, const double &number)
{
  return vector_3d(number * vector.x, number * vector.y, number * vector.z);
}

// Умножение на число слева
inline const vector_3d operator *(const double &number, const vector_3d &vector)
{
  return vector_3d(number * vector.x, number * vector.y, number * vector.z);
}

// Деление вектора на число. Проверка деления на 0 не производится!
inline const vector_3d operator /(const vector_3d &vector, const double &number)
{
  return vector_3d(vector.x / number, vector.y / number, vector.z / number);
}

// Скалярное произведение двух векторов
inline double operator *(const vector_3d &left, const vector_3d &right)
{
  return left.x * right.x + left.y * right.y + left.z * right.z;
}

// Векторное произведение (для правой системы координат)
inline vector_3d operator %(const vector_3d &left, const vector_3d &right)
{
  return vector_3d(left.y * right.z - left.z * right.y, left.z * right.x - left.x * right.z, left.x * right.y - left.y * right.z);
}

// Операция неравно для векторов. true, если векторы не равны.
inline bool operator != (const vector_3d &left, const vector_3d &right)
{
  return fabs (right.x - left.x) > gl::EPS || fabs (right.y - left.y) > gl::EPS || fabs (right.z - left.z) > gl::EPS;
}

// Операция проверки на равенство двух векторов. Сравнение с заданной точностью EPS.
inline bool operator == (const vector_3d &left, const vector_3d &right)
{
  return fabs (right.x - left.x) < gl::EPS && fabs (right.y - left.y) < gl::EPS && fabs (right.z - left.z) < gl::EPS;
}

#endif // VECTOR_3D_H
