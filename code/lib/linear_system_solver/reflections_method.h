#ifndef REFLECTIONS_METHOD_H
#define REFLECTIONS_METHOD_H

#include <iostream>
#include <math.h>

int get_reflection_vector(double *matrix, int size_matrix, double *reflection_vector, int position);
int multiplacate_reflection_vector_and_vector(double *vector, int size_vector, int size_matrix, double *reflection_vector);
int multiplacate_reflection_vector_and_matrix(double *matrix, int size_matrix, double *reflection_vector, int position);
int multiplacate_reflection_vector_and_adjoint_matrix(double *matrix, int size_matrix, double *reflection_vector, int position);
int reflections_method_solver(double *matrix, int size_matrix, double *right_hand_member, double *result);
double residual(double *matrix, int size_matrix, double *right_hand_member, double *result);

// Search inverse matrix using the method of reflections.
int reflections_method_inverse_matrix(double *matrix, int size_matrix, double *result);

#endif // REFLECTIONS_METHOD_H
