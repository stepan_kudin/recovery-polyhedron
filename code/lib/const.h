#ifndef CONST_H
#define CONST_H

#include <math.h>

const double EPS = 1e-10;
const double ASSUMPTIVE_ERROR = 5e-3;
const double SENSITIVITY_DUPLICATES = 1e-8;
const double DISTANCE_NEAR_CLUSTERS = 3e-2;


const double PI = 3.14159265358979323846;
const double SCAN_ERROR = 1e-3;
const double TAU = 2 * SCAN_ERROR;
const unsigned int CRITERION_NOISE = 1;
const double MAX_SCAN_ANGLE = PI / 10.0;
const double FOREL_CRITERION = 2 * SCAN_ERROR * (1.0 / sin(0.5 * (PI - MAX_SCAN_ANGLE)));

#endif // CONST_H
