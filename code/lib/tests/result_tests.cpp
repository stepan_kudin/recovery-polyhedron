#include "result_tests.h"

void compare_cluster_center_and_vertexes_original_model(std::vector<shot_model> model_shots,
                                                        std::vector<std::vector< std::pair<unsigned int, unsigned int> > > clusters,
                                                        std::vector<vector_3d> &original_model_vertexes,
                                                        double &max_dist, double &min_dist)
{
  unsigned int index_1, index_2;
  double distance_buffer;

  // Получим список центров кластеров
  vector_3d *centres_clusters = new vector_3d[clusters.size()];
  for(index_1 = 0; index_1 < clusters.size(); ++index_1)
    centres_clusters[index_1] = search_cluster_center(model_shots, clusters[index_1]);

  // Посчитаем максимальное и минимальное отклонение между центрами кластеров и образцом
  max_dist = -1.0;
  min_dist = 1000.0;

  for(index_1 = 0; index_1 < clusters.size(); ++index_1)
    {
      for(index_2 = 0; index_2 < original_model_vertexes.size(); ++index_2)
        {
          distance_buffer = (original_model_vertexes[index_2] - centres_clusters[index_1]).length();

          if(distance_buffer < min_dist)
            min_dist = distance_buffer;
        }

      if(min_dist > max_dist)
        max_dist = min_dist;
    }

  delete[] centres_clusters;
}
