#ifndef RESULT_TESTS_H
#define RESULT_TESTS_H

#include <vector>

#include "../geometry_lib/vector_3d.h"
#include "../structures/shot_model.h"
#include "../functions/lines_classifier.h"

void compare_cluster_center_and_vertexes_original_model(std::vector<shot_model> model_shots,
                                                        std::vector<std::vector< std::pair<unsigned int, unsigned int> > > clusters,
                                                        std::vector<vector_3d> &original_model_vertexes, double &max_dist, double &min_dist);

#endif // RESULT_TESTS_H
