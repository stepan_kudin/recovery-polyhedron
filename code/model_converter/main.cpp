#include <iostream>
#include <stdio.h>
#include <string.h>

const int MAX_BUFF_LEN = 256;

// На вход даётся файл с моделью и имя для .ply файла, который будет сгенерирован
int main(int argc, char* argv[])
{
  if(argc != 3)
    {
      printf("Usage: %s <model_name> <output_file_name>\n", argv[0]);
      return -1;
    }

  // Откроем файлы
  FILE *in = fopen(argv[1], "r");
  if(in == NULL)
    {
      printf("Couldn't open file \"%s\"!", argv[1]);
      return -2;
    }

  char output_file_name[MAX_BUFF_LEN];
  strcpy(output_file_name, argv[2]);
  strcat(output_file_name, ".ply");
  FILE *out = fopen(output_file_name, "w");
  if(out == NULL)
    {
      printf("Couldn't open file \"%s\"", output_file_name);
      return -3;
    }

  printf("Convert is started!\n");

  // Запишем заголовки .ply файла
  fprintf(out, "ply\n");
  fprintf(out, "format ascii 1.0\n");


  // Прочитаем количество вершин, граней, рёбер в модели
  int count_vertexes, count_facets, count_edges;

  fscanf(in, "%d %d %d", &count_vertexes, &count_facets, &count_edges);

  // Запишем информацию в ply файл
  fprintf(out, "element vertex %d\n", count_vertexes);

  // Описание координат вершин
  fprintf(out, "property double x\n");
  fprintf(out, "property double y\n");
  fprintf(out, "property double z\n");

  // Запишем количество граней
  fprintf(out, "element face %d\n", count_facets);

  // Описание описания грани, как списка вершин
  fprintf(out, "property list uchar int vertex_index\n");

  // Конец заголовка
  fprintf(out, "end_header\n");

  // Запишем вершины
  int index;
  double x, y, z;
  int int_buff;
  for(index = 0; index < count_vertexes; ++index)
    {
      fscanf(in, "%d %lf %lf %lf", &int_buff, &x, &y, &z);
      fprintf(out, "%lf %lf %lf\n", x, y, z);
    }

  // Запишем грани
  int count_vertexes_in_face;
  double double_buff;
  char char_buff;
  for(index = 0; index < count_facets; ++index)
    {
      fscanf(in, "%d", &int_buff);
      fscanf(in, "%d", &count_vertexes_in_face);
      fscanf(in, "%lf %lf %lf %lf", &double_buff, &double_buff, &double_buff, &double_buff);

      fprintf(out, "%d ", count_vertexes_in_face);

      for(int facets_iterator = 0; facets_iterator < count_vertexes_in_face; ++facets_iterator)
        {
          fscanf(in, "%d", &int_buff);
          fprintf(out, "%d ", int_buff);
        }
      fprintf(out, "\n");
      /*fscanf(in, "%c", &char_buff);
      fscanf(in, "%d", &int_buff);
      fscanf(in, "%c", &char_buff);*/
    }

  fclose(in);
  fclose(out);

  printf("Convert is finished!\n");

  return 0;
}

