#include <stdio.h>

#include <string>
#include <vector>
#include <list>

#include "lib/io/shadow_contours_reader.h"

#include "lib/structures/polyhedra_model.h"

#include "lib/functions/shadow_contours_vertexes_diagram.h"
#include "lib/functions/diagram_clasterization.h"
#include "lib/functions/finding_vertices_polyhedron.h"

#include "lib/checker/checker_vertices.h"

int main(int argc, char *argv[])
{
  if(argc != 3)
    {
      printf("Usage: %s <file with shadows> <file with original model>\n", argv[0]);
      return -1;
    }
  char *shadows_filename = argv[1];
  char *model_name = argv[2];

  printf("Reading shadows begin...\n");

  std::vector<shot_model> shots;

  if(read_shot_model(shadows_filename, shots))
    {
      printf("Error reading shadows!\n");
      return -1;
    }

  printf("Readng shadows complete!\n");

  printf("\n\n");

  printf("Reading original model begin...\n");

  polyhedra_model original_model;

  if(original_model.read_model(model_name))
    {
      printf("Error reading original model!\n");
      return -2;
    }

  printf("Reading original model complete!\n");

  printf("\n\n");

  printf("The beginning of construction of diagrams.\n");

  std::vector<std::vector<diagram_point> > diagram = create_shadow_contours_vertexes_diagram(shots);

  std::string diagram_filename(shadows_filename);
  diagram_filename += "_diagram.nb";

  if(draw_diagram(diagram, diagram_filename.c_str()))
    printf("Error opening the file.\n");

  printf("Diagram construction completed.\n");

  printf("Diagram clusterization begin...\n");

  std::vector<std::vector<frame_vertex_t>> frame_vertex_marks = frame_vertex_marking(diagram, shots);

  print_frame_vertex_marks(frame_vertex_marks);

  std::list<std::vector<frame_point>> diagram_clusters = diagram_clusterization(diagram, shots, frame_vertex_marks);

  unsigned int count_deleted_clusters;
  delete_noise(diagram_clusters, count_deleted_clusters);
  printf("Deleted noise clusters: %d\n", count_deleted_clusters);

  std::string diagram_clusters_filename(shadows_filename);
  diagram_clusters_filename += "_diagram_clusters.nb";

  if(draw_diagram_clusterization(diagram_clusters, shots, diagram_clusters_filename.c_str()))
    {
      printf("Couldn't open %s", diagram_clusters_filename.c_str());
    }

  printf("Diagram clusterization completed.\n");

  printf("Find vertices...\n");

  std::list<vertex_cluster> vertices = find_vertices(diagram_clusters, shots);

  printf("Find vertices complite!\n");

  printf("\n\n");

  printf("Checking vertices begin...\n\n");

  double min_dist, max_dist;

  check_vertices(vertices, &original_model, min_dist, max_dist);

  printf("Min distance: %.16lf\n", min_dist);
  printf("Max distance: %.16lf\n", max_dist);
  printf("Not found vertices: %u\n", original_model.vertices.size() - vertices.size());

  printf("Checking vertices complete!\n");

  return 0;
}
