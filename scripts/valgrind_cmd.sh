#!/bin/bash

# This script runs valgrind (more precisely, its memcheck tool) with standard options

sudo valgrind --trace-children=yes --db-attach=yes --track-origins=yes --leak-check=full $@
